
/*
 * Der nachfolgende Code basiert auf einem Beispiel von NVIDIA. 
 *
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 */

// System includes
#include <stdio.h>
//#include <assert.h>

// CUDA runtime
//#include <cuda_runtime.h>

// helper functions and utilities to work with CUDA
//#include <helper_functions.h>  // CUDA 5.0 only
//#include <helper_cuda.h>	// CUDA 5.0 only
//#include <cutil.h>	// CUDA <5.0 only

// This kernel computes a standard parallel reduction 
__global__ static void simpleReduction(const float *input, float *output)
{
    extern __shared__ float shared[];

    const int tid = threadIdx.x;
    const int bid = blockIdx.x;

    // Copy input.
    shared[tid] = input[tid + bid * blockDim.x * 2];
    shared[tid + blockDim.x] = input[tid + blockDim.x + bid * blockDim.x * 2];

    // Perform reduction to find minimum.
    for (int d = blockDim.x; d > 0; d /= 2)
    {
        __syncthreads();

        if (tid < d)
        {
            float f0 = shared[tid];
            float f1 = shared[tid + d];

            if (f1 < f0)
            {
                shared[tid] = f1;
            }
        }
    }

    // Write result.
    if (tid == 0) output[bid] = shared[0];

    //__syncthreads();

}


// Main function starts here.

#define NUM_BLOCKS    8
#define NUM_THREADS   256

// Main function starts here
int main(int argc, char **argv)
{
    
    float *dinput = NULL;
    float *doutput = NULL;

    float input[NUM_THREADS * 2 * NUM_BLOCKS];
	float output[NUM_BLOCKS];

    for (int i = 0; i < NUM_THREADS * 2 * NUM_BLOCKS; i++)
    {
        input[i] = (float)i+1;
    }

    cudaMalloc((void **)&dinput, sizeof(float) * NUM_THREADS * 2 *NUM_BLOCKS);
    cudaMalloc((void **)&doutput, sizeof(float) * NUM_BLOCKS);
    
    cudaMemcpy(dinput, input, sizeof(float) * NUM_THREADS * 2 * NUM_BLOCKS, cudaMemcpyHostToDevice);

    simpleReduction<<<NUM_BLOCKS, NUM_THREADS, sizeof(float) * 2 * NUM_THREADS>>>(dinput, doutput);

    cudaMemcpy(output, doutput, sizeof(float) * NUM_BLOCKS, cudaMemcpyDeviceToHost);

    cudaFree(dinput);
    cudaFree(doutput);
    
	float min = output[0];
    for (int i = 1; i < NUM_BLOCKS; i++)
    {
		printf("Block min: %f\n", output[i]);
		if (output[i] < min)
			min = output[i];
    }
	printf("Overall min: %f\n", min);

	char c;
	scanf(&c);

    return EXIT_SUCCESS;
}
