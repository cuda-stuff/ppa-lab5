
#include <stdio.h>

__global__ void saxpy(int n, float a, float *x, float *y)
{
  int i = blockIdx.x*blockDim.x + threadIdx.x;
  // Pr�fen ob noch Daten / werte zum Berechnen / "innerhalb" Vektor vorhanden
  if (i < n) 
  {
	  // result should be 4.0 if a is 2.0
	  y[i] = a*x[i] + y[i];
  }
}

int main(void)
{
  int N = 1<<20;
  N = 10;
  printf("Create two vectors of size %i.\n", N);
  float *x, *y, *d_x, *d_y;
  x = (float*)malloc(N*sizeof(float));
  y = (float*)malloc(N*sizeof(float));

  for (int i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }

  printf("Allocate memory on device.\n");
  cudaMalloc((void**)&d_x, N*sizeof(float)); 
  cudaMalloc((void**)&d_y, N*sizeof(float));

  
  printf("Copy input vectors to device... ");
  cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);
  printf("done.\n");

  // Perform SAXPY on 1M elements
  printf("Launching kernel... \n");
  saxpy<<<(N + 1) / 2, 2>>>(N, 2.0, d_x, d_y);
  // saxpy<<<(N)/256, 256>>>(N, 2.0, d_x, d_y);

  printf("Kernel finished.\n");

  printf("Copy result back to host... ");
  cudaMemcpy(y, d_y, N*sizeof(float), cudaMemcpyDeviceToHost);
  printf("done.\n");

  float maxError = 0.0f;
  float expectedResult = 4.0f;
  for (int i = 0; i < N; i++) {
	  maxError = max(maxError, abs(y[i] - expectedResult));
  }
  printf("Max error: %f\n", maxError);
  
  char c;
  scanf(&c);
}
